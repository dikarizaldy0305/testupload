package UploadAC;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class KonversiUang {
    private double nilai;
    private double hasil;
    private double nilaiDollar = 14350;

    //Ini constructor konversi uang
    KonversiUang(){
        nilai =1;
        hasil = nilaiDollar;
    }
    KonversiUang(double uang){
        nilai = uang;
        hasil = nilai * nilaiDollar;
    }
    KonversiUang(double uang, double dollar){
        nilai = uang;
        nilaiDollar = dollar;
        hasil =  nilai * nilaiDollar;
    }
    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    public void setNilaiDollar(double nilaiDollar) {
        this.nilaiDollar = nilaiDollar;
    }
    public double getNilai() {
        return nilai;
    }
    public double getNilaiDollar() {
        return nilaiDollar;
    }
    public double getHasil() {
        return hasil;
    }

    public static void main(String[] args) {
        KonversiUang uang = new KonversiUang();
        NumberFormat format = new DecimalFormat("#0.00");
        double money, money2;
        Scanner scanner = new Scanner(System.in);
        System.out.print("==== Program Konversi Uang USD to RP====\n");
        System.out.print("Nilai Dollar saat ini : "+uang.getNilai() +" USD = Rp. " + uang.getHasil());
        System.out.print("\nMasukkan uang (dalam dollar) : USD. ");
        money= scanner.nextDouble();
        KonversiUang uang2 = new KonversiUang(money);
        System.out.println("\nHasil konversi (dollar statis) : Rp. "+ uang2.getHasil());
        System.out.println("==== nilai uang dan dollar Dinamis ===");
        System.out.print("Masukkan uang (dalam dollar) : USD. ");
        money = scanner.nextDouble();
        System.out.print("\nMasukkan nilai dollar (rupiah) : Rp. ");
        money2 = scanner.nextDouble();
        KonversiUang uang3 = new KonversiUang(money, money2);
        System.out.println("Hasil Konversi (dalam rupiah) : Rp." + uang3.getHasil());
    }
}

